package sk.ite.got.castle.infrastructure.messaging;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import sk.ite.got.castle.domain.event.CastleCreatedEvent;

/**
 * Created by macalaki on 31.01.2017.
 */
@MessagingGateway
public interface CastleEventGateway {

    @Gateway(requestChannel = MessageChanelInterface.CASTLE_SOURCE)
    void sendCastleCreatedEvent(CastleCreatedEvent event);
}

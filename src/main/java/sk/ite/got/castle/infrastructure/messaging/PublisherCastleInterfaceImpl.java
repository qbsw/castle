package sk.ite.got.castle.infrastructure.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;
import sk.ite.got.castle.domain.event.CastleCreatedEvent;

/**
 * Created by macalaki on 31.01.2017.
 */

@Service
public class PublisherCastleInterfaceImpl implements PublisherCastleInterface {
    @Autowired
    private MessageChanelInterface publisher;

    @Override
    public void sendCastleCreatedEvent(CastleCreatedEvent event) {
        publisher.sendCastleCreatedEvent().send(
                MessageBuilder.withPayload(event)
                        .setHeader("SOME_HEADER", "value")
                        .build());
    }
}

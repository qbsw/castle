package sk.ite.got.castle;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import sk.ite.got.castle.infrastructure.messaging.MessageChanelInterface;

@Configuration
@EnableBinding(MessageChanelInterface.class)
@IntegrationComponentScan
public class MessageChanelConfiguration {
	

}

package sk.ite.got.castle.application.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sk.ite.got.castle.application.dto.DTOCastle;
import sk.ite.got.castle.domain.event.CastleCreatedEvent;
import sk.ite.got.castle.domain.model.Castle;
import sk.ite.got.castle.infrastructure.messaging.CastleEventGateway;
import sk.ite.got.castle.infrastructure.messaging.PublisherCastleInterface;
import sk.ite.got.castle.infrastructure.persistence.CastleRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * CastleService implementation.
 *
 * @author macalak@itexperts.sk
 *
 */
public class MyFeatureBean {
	
	public String  doStuff() {
		return "Doing stuff...";
	}

}

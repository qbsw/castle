package sk.ite.got.castle.application.service;

import org.modelmapper.ModelMapper;
import sk.ite.got.castle.application.dto.DTOCastle;
import sk.ite.got.castle.domain.event.CastleCreatedEvent;
import sk.ite.got.castle.domain.model.Castle;
import sk.ite.got.castle.infrastructure.messaging.CastleEventGateway;
import sk.ite.got.castle.infrastructure.messaging.PublisherCastleInterface;
import sk.ite.got.castle.infrastructure.persistence.CastleRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.*;

/**
 * CastleService implementation.
 *
 * @author macalak@itexperts.sk
 *
 */
@Service
@Transactional(readOnly=true)
public class CastleServiceBean implements CastleService {
	
	@Autowired
	private CastleRepository castleRepository;
	@Autowired
	ModelMapper dtoMapper;
	@Autowired
	PublisherCastleInterface publisherCastle;
	@Autowired
	private CastleEventGateway castleEventGateway;


	@Override
	public List<DTOCastle> getCastles() {
		List<Castle> castles= castleRepository.findAll();
		DTOCastle dTOCastle=dtoMapper.map(castles.get(0),DTOCastle.class);
		return StreamSupport.stream(castleRepository.findAll().spliterator(), false)
				.map(p -> dtoMapper.map(p, DTOCastle.class))
				.collect(Collectors.toList());
	}

	@Override
	public void createCastle(DTOCastle castle) {
		Castle savedCastle = castleRepository.save(dtoMapper.map(castle, Castle.class));

//		publisherCastle.sendCastleCreatedEvent(new CastleCreatedEvent(savedCastle.getId(),
//				savedCastle.getName(),
//				savedCastle.getRuler(),
//				savedCastle.getLocation()));
		castleEventGateway.sendCastleCreatedEvent(new CastleCreatedEvent(savedCastle.getId(),
				savedCastle.getName(),
				savedCastle.getRuler(),
				savedCastle.getLocation()));
	}
}

package sk.ite.got.castle.application.service;

import sk.ite.got.castle.application.dto.DTOCastle;

import java.util.List;

/**
 * CastleService interface.
 *
 * @author macalak@itexperts.sk
 *
 */
public interface CastleService {
	
	List<DTOCastle> getCastles();
	void createCastle(DTOCastle castle);

}

package sk.ite.got.castle.application.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sk.ite.got.castle.application.service.MyFeatureBean;

/**
 * CastleService implementation.
 *
 * @author macalak@itexperts.sk
 *
 */
public class MyFeatureConfiguration {

	@Bean
	public MyFeatureBean getMyFeatureBean(){
		return new MyFeatureBean();
	}
}

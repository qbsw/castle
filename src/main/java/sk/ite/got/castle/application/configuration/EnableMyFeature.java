package sk.ite.got.castle.application.configuration;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * CastleService implementation.
 *
 * @author macalak@itexperts.sk
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(MyFeatureConfiguration.class)
public @interface EnableMyFeature {

}

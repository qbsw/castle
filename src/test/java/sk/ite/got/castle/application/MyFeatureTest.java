package sk.ite.got.castle.application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import sk.ite.got.castle.application.configuration.EnableMyFeature;
import sk.ite.got.castle.application.service.MyFeatureBean;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static org.junit.Assert.assertNotNull;

/**
 * CastleService implementation.
 *
 * @author macalak@itexperts.sk
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyFeatureTest{

    @Autowired
    MyFeatureBean myFeatureBean;

    @Test
    public void testMyFeature(){
        assertNotNull(myFeatureBean);
        System.out.println(myFeatureBean.doStuff());
    }

}